
public class ProjectEuler10 {

	public static void main(String[] args) {
		
		long s=2,n=3;
		while (n<2000000)
		{
			if(isPrime(n))
				s += n;
			n++;
		}
		System.out.println(s);

	}
	public static boolean isPrime(long n)
	{
		int i=2;
		boolean ok=true;
		while(ok&&i<=Math.sqrt(n))
			{
			if(n%i==0)
				return false;
			else
				i++;
			}
		return ok;		
		
	}

}
